(* Proper class with one method *)
class A {
	ana(): Int {
		(let x:Int <- 1 in 2)+3
	};
};

(* Proper class with one method one arg *)
Class BB__ inherits A {
	one_arg(arg1:Int): Int {
		self
	};
};

(* Class with one attribute *)
class Test3 {
	attr1 : Int;
};

(* Class with one attribute one method *)
class Test4 {
	attr1 : Int;

	one_method() : Int {
		self
	};
};

(* Class with many attributes and many fields *)
class Test5 {

	attr1 : Int;
	attr2 : Int;
	attr3 : Int;
	attr4 : Int;
	attr5 : Int;


	one_method() : Int {
		self
	};
	two_method() : Int {
		self
	};
	three_method() : Int {
		self
	};
	four_method() : Int {
		self
	};
	five_method() : Int {
		self
	};

};

(* Class with many attributes and many fields, mixed *)
class Test6 {

	attr1 : Int;
	attr5 : Int;


	one_method() : Int {
		self
	};

	attr2 : Int;

	two_method() : Int {
		self
	};
	three_method() : Int {
		self
	};

	attr3 : Int;
	attr4 : Int;

	four_method() : Int {
		self
	};

	five_method() : Int {
		self
	};

};


(* Testing assign types are correct *)
class Test {
	attr1 : Int <- 89;
	attr2 : Int <- 5;

	method_one () : Object {
		attr1 <- 5
	};

	method_two () : Int {
		x <- y <- 5
	};
};



(* This section will test proper expressions *)

(* This class tests dispatch calls *)
class Test {
	-- Dispatch with one argument
	valid_single(attr1 : Int) : Int {
		x(1)
	};

	-- Dispatch with multiple arguments
	valid_multiple(attr1 : Int) : Int {
		x(1,2,3,4)
	};

	-- Dispatch with no arguments
	valid_noargs(attr1 : Int, attr2 : Int, attr3 : Int) : Int {
		x.type()
	};

};

(* This class tests if statements of various forms *)
class Test {

	-- IF/ELSE reference block
	method_one(attr1 : Int) : Int {
		if attr1 = 1 then {1; 2;} else true fi
	};

	-- Nested IF/ELSE
	method_two(attr1: Int) : Int {
		if attr1 = 1 then {if attr1 < 1 then isvoid 5 else new Int fi;} else true fi
	};
};

(* This class will test all arithmetic precedence *)
class Test {
	arith_test() : Int {
		{
		a+b+c;
		a-b-c;
		a+b-c;
		a-b+c;

		a+b*c;
		a+b/c;
		a-b*c;
		a-b/c;
		a*b+c;
		a/b+c;
		a*b-c;
		a/b-c;

		a*b/c;
		a/b*c;
		a*b*c;
		a/b/c;
		}
	};
};

(* This class will test all arithmetic associativity *)
class Test {
	attr1 : Int;
	attr2 : Int;
	attr3 : Int;
	attr4 : Int;

	associativity_plus () : Test {
		a + b + c + d
	};

	associativity_minus () : Object {
		a - b - c - d
	};

	associativity_multiplication () : String {
		a * b * c * d
	};

	associativity_division () : Test {
		a / b / c / d
	};
};

(* Testing a case expression with many branches *)
class Test {
	case_test() : Int {
		case 5 of case1 : String => 1;
				  case2 : Int => 2;
				  case3 : Test => 3;
		esac
	};
};

(* Testing a case with complex expressions *)
class Test {
	case_test() : Int {
		case 5 of case1 : String => 1;
				  case2 : Int => {1; 2;};
				  case3 : Test => 3;
		esac
	};
};

(* Testing two ADJACENT added Let expressions *)
class Test {
	let_test() : Int {
		let val1 : Int <- 5 in let val2 : String in val2
	};
};



(* Testing valid comparisons - should be associative *)
class Test {
	method_one(x : Int, y : Int) : Int {
		{ x = (3 < y) ;}
	};
};