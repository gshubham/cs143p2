(*
 *  execute "coolc bad.cl" to see the error messages that the coolc parser
 *  generates
 *
 *  execute "myparser bad.cl" to see the error messages that your parser
 *  generates
 *)

(* no error *)
class A {
};

(* error:  b is not a type identifier *)
Class b inherits A {
};

(* error:  a is not a type identifier *)
Class C inherits a {
};

(* error:  keyword inherits is misspelled *)
Class D inherts A {
};

(* error:  closing brace is missing *)
Class E inherits A {
;

(* Class with no name *)
class {
	attr1 : Int;
};

(* The follow section tests for errors in feature lists *)

(* Feature name is incorrect without arg *)
class Test1 {
	Capital_letter : String {
		self
	};
};

(* Feature name is incorrect with arg *)
class Test2 {
	Capital_letter(arg1 : Int) : String {
		self
	};
};

(* Attributes are not separated by semicolons *)
class Test3 {
	attr1 : Int
	attr2 : String

	method_one(arg1 : Int) : Int {
		1
	};
};

(* Attribute does not have a proper TYPEID *)

class Test4 {
	attr1 : Int;
	Attr2 : Int;
};

(* This section tests incorrect uses of GET *)
class Test {
	method_one(a : int) : Int {
		a <- ClassName
	};
};

(* This section tests bad expressions *)

(* Expression separated by semicolon but not in list *)
class Test {
	method_one() : Int {
		5;
	};
};

(* Expression block used incorrectly with extra ';' *)
class Test {
	broken_block() : Int {
		{
			1;
			2;
			3;
			4;
		};
	};

	recovery() : Int {
		1
	};
};

(* Bad dispatch calls *)
class Test {
	-- Expression list incomplete
	method_one(attr1 : Int) : Int {
		attr1(1, 2, 3,)
	};

	-- Express list incorrect
	method_two(attr1 : Int) : Int {
		attr1(,1,2,)
	};

	-- Expression list separated by semicolons
	method_three(attr1: Int) : Int {
		attr1(1;2;3;)
	};

	recovery() : Int {
		1
	};
};

(* Bad calls to if/else *)
class Test {
	-- IF but no FI
	method_one(attr1 : Int) : Int {
		if false then isvoid 5 else 1
	};

	-- IF no ELSE
	method_zero(attr1 : Int) : Int {
		if attr1 > 0 then false fi
	};

	-- IF missing THEN
	method_two(attr1 : Int) : Int {
		if false then else 1 fi
	};

	recovery() : Int {
		1
	};	
};

(* This block checks for a bad expression list *)
class Test {
	-- Empty block is not valid
	method_one() : Int { {} };

	-- Empty block with only semicolon is not valid
	method_two() : Int { {;} };

	recovery() : Int {
		1
	};
};


(* This class tests for incorrect calls to case *)
class Test {
	attr1 : Int <- 1;
	attr2 : Int <- 1;

	-- Calling a case with no expression
	method_one() : Int {
		case of
			a : Int => 1;
			b : Object => 2;
	};


	recovery() : Int {
		1
	};
};